# sample-spring

[![GitHub release](https://img.shields.io/github/release/nalbam/sample-spring.svg)](https://github.com/nalbam/sample-spring/releases)
[![Build-Push](https://github.com/nalbam/sample-spring/actions/workflows/push.yaml/badge.svg)](https://github.com/nalbam/sample-spring/actions/workflows/push.yaml)
[![CircleCI](https://circleci.com/gh/nalbam/sample-spring.svg?style=svg)](https://circleci.com/gh/nalbam/sample-spring)

[![DockerHub Badge](http://dockeri.co/image/nalbam/sample-spring)](https://hub.docker.com/r/nalbam/sample-spring/)

## Docker

```bash
docker pull nalbam/sample-spring
```
